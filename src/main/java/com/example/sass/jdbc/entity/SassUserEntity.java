package com.example.sass.jdbc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.sass.jdbc.comment.CreateDateBaseComment;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("sass_user")
public class SassUserEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer Id;

    /**
     * 租户编号
     */
    @TableField("sass_user_no")
    private String sassUserNo;

    /**
     * 租户名称
     */
    @TableField("sass_name")
    private String sassName;

    /**
     * 租户账号
     */
    @TableField("sass_user_name")
    private String sassUserName;

    /**
     * 租户密码
     */
    @TableField("sass_password")
    private String sassPassword;

    /**
     * 租户数据库url:端口
     */
    @TableField("sass_db_url")
    private String sassDbUrl = CreateDateBaseComment.m_url;

    /**
     * 租户数据库名称
     */
    @TableField("sass_db_name")
    private String sassDbName = CreateDateBaseComment.m_dbName;

    /**
     * 租户数据库用户名
     */
    @TableField("sass_db_user")
    private String sassDbUser = CreateDateBaseComment.m_user;

    /**
     * 租户数据库密码
     */
    @TableField("sass_db_password")
    private String sassDbPassword = CreateDateBaseComment.m_password;

}
