package com.example.sass.jdbc.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class DateBaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * sql 文件
     */
    private  String  db_scriptFilePath;

    /**
     * 数据库服务器地址和端口
     */
    private  String db_url;

    /**
     * 数据库名称
     */
    private  String db_dbName;

    /**
     * 数据库用户名
     */
    private  String db_user;

    /**
     * 数据库密码
     */
    private  String db_password;
}
