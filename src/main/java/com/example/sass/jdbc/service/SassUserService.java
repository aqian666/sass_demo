package com.example.sass.jdbc.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.sass.jdbc.entity.SassUserEntity;
import com.example.sass.jdbc.mapper.SassUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SassUserService {

    @Autowired
    private SassUserMapper sassUserMapper;


    public void save(SassUserEntity sassUser) {
        sassUserMapper.insert(sassUser);
    }

    public List<SassUserEntity> listUser() {
        return sassUserMapper.selectList(new QueryWrapper<>());
    }
}
