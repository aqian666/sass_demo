package com.example.sass.jdbc.contorller;

import com.example.sass.jdbc.service.SassUserService;
import com.example.sass.jdbc.comment.CreateDateBaseComment;
import com.example.sass.jdbc.entity.DateBaseEntity;
import com.example.sass.commen.R;
import com.example.sass.jdbc.entity.SassUserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("dbCreate")
public class CreateDateBaseController {


    @Autowired
    private CreateDateBaseComment createDateBaseComment;
    @Autowired
    private SassUserService sassUserService;


    @PostMapping("/create")
    public R findGoods(SassUserEntity sassUser){
        DateBaseEntity dateBase = new DateBaseEntity();
        dateBase.setDb_url(sassUser.getSassDbUrl()+"/"+sassUser.getSassDbName());
        dateBase.setDb_dbName(sassUser.getSassDbName());
        dateBase.setDb_user(sassUser.getSassDbUser());
        dateBase.setDb_password(sassUser.getSassDbPassword());
        if (createDateBaseComment.creatDB(dateBase)){
            //保存创建记录
            sassUserService.save(sassUser);
        }
        return R.ok();
    }


}
