package com.example.sass.jdbc.contorller;

import com.example.sass.base.entity.User;
import com.example.sass.base.service.UserService;
import com.example.sass.commen.R;
import com.example.sass.jdbc.service.SassUserService;
import com.example.sass.jdbc.config.datasource.ChangeDataSourceService;
import com.example.sass.jdbc.entity.SassUserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("dbSwitch")
public class SwitchDateBaseController {

    @Autowired
    private ChangeDataSourceService changeDataSourceService;
    @Autowired
    private UserService userService;

    @PostMapping("switch")
    private R test(String datasourceId){
        changeDataSourceService.changeDS(datasourceId);
        List<User> list1 = userService.listUser();
        System.out.println(list1);

        //切回主数据源
//        DataSourceContextHolder.removeDataSource();
        return R.ok(list1);
    }
}
