package com.example.sass.jdbc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.sass.jdbc.entity.SassUserEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SassUserMapper extends BaseMapper<SassUserEntity> {
}
