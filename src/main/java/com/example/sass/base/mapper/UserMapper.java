package com.example.sass.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.sass.base.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jishuqin
 * @since 2022-12-14
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}
