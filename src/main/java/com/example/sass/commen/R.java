package com.example.sass.commen;

import lombok.Data;

@Data
public class R {
    private int code; // 编码 200/400
    private String msg; // 成功、失败
    private Long total; // 总条数
    private Object data; // 数据

    public static R fail(){
        return result(400,"失败",0L,null);
    }

    public static R fail(int code){
        return result(code,"失败",0L,null);
    }

    public static R fail(String msg){
        return result(400,msg,0L,null);
    }

    public static R ok(){
        return result(200 ,"成功",0L,null);
    }

    public static R ok(Object data){
        return result(200,"成功",0L,data);
    }

    public static R ok(Object data, Long total){
        return result(200 ,"成功",total,data);
    }

    public static R result(int code, String msg, Long total, Object data){
        R res = new R();
        res.setData(data);
        res.setMsg(msg);
        res.setCode(code);
        res.setTotal(total);
        return res;
    }
}
