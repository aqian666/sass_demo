package com.example.test.business;

import com.example.test.dao.UserDaoImpl;
import com.example.test.pojo.User;

import java.util.Scanner;

public class Business {

    private static User user;

    public static void main(String[] args) {
        UserDaoImpl ud = new UserDaoImpl();

        outerLoop : while (true) {
            System.out.println("-----------Welcome-----------");
            System.out.println("1 登录");
            System.out.println("2 注册");
            System.out.println("3 退出");
//            System.out.print("请选择要执行的功能：");
            Scanner sc = new Scanner(System.in);
            String choice = sc.nextLine();
            switch (choice) {
                case "1":
                    // 登录功能
                    System.out.println("-----------Login-----------");
                    System.out.print("请输入用户名：");
                    String username = sc.nextLine();
                    System.out.print("请输入密码：");
                    String password = sc.nextLine();

                    if (ud.isLogin(username, password)) {
                        System.out.println("-----------登录成功！-----------");

                        user = ud.findUser(username);

                        inLoop : while (true) {
                            System.out.println("-----------请选择功能点！-----------");
                            System.out.println("1 我的个人信息");
                            System.out.println("2 车辆信息");
                            System.out.println("3 我的订单");

                            String c = sc.nextLine();
                            switch (c) {
                                case "1":
                                    userinfo(ud,sc,user);
                                    break;
                                case "2":
                                    carinfo(ud,sc,user);
                                    break;
                                case "3":
                                    break;
                            }
                        }





                    } else {
                        System.out.println("用户名或密码错误！请重新登录！");
                    }
                    break;
                case "2":
                    // 注册功能
                    System.out.println("-----------Regist-----------");
                    System.out.print("请输入用户名：");
                    String newUsername = sc.nextLine();
                    System.out.print("请输入密码：");
                    String newPassword = sc.nextLine();
                    System.out.print("请输入名字：");
                    String newName = sc.nextLine();
                    User newUser = new User();
                    newUser.setUsername(newUsername);
                    newUser.setPassword(newPassword);
                    newUser.setName(newName);
                    // 注册信息写入
                    System.out.println(ud.regist(newUser));
                    break;
                case "3":
                default:
                    System.out.println("-----------Bye~-----------");
                    System.exit(0);
                    break;
            }
        }
    }

    private static void carinfo(UserDaoImpl ud, Scanner sc, User user) {
    }

    private static void userinfo(UserDaoImpl ud,Scanner sc,User olduser) {
        interLoop: while (true) {
            System.out.println("1 查看个人信息");
            System.out.println("2 编辑个人信息");
            System.out.println("3 删除个人信息");
            String userInfo = sc.nextLine();

            switch (userInfo) {
                case "1":
                    //查看个人信息
                    System.out.println("账号："+olduser.getUsername()+"密码："+olduser.getPassword()+"名称:"+olduser.getName());
                    break;

                case "2":
                    //编辑个人信息
                    System.out.print("请输入新名字：");
                    String up_name = sc.nextLine();
                    olduser.setName(up_name);
                    ud.updateUser(olduser);
                    break;

                case "3":
                    //删除个人信息
                    System.out.println("确认删除吗？");
                    System.out.println("1 是");
                    System.out.println("2 否");
                    String del_name = sc.nextLine();
                    if (del_name.equals("1")){
                        ud.delUser(olduser.getUsername());
                    }
                    break interLoop;
            }
        }
    }

}
