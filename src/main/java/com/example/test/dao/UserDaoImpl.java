package com.example.test.dao;

import com.example.test.pojo.User;

import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl {

    public List<User> users = new ArrayList<>();
    public boolean isLogin(String username, String password) {
        boolean flag = false;
        for (User i : users) {
            if (username.equals(i.getUsername()) && password.equals(i.getPassword())) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    public String regist(User user) {
        if (users.size()>0 && findUser(user.getUsername())!=null){
            return "用户名已存在";
        }
        users.add(user);
        return "注册成功";
    }

    public User findUser(String username){
        for (User user1 : users) {
            if(user1.getUsername().equals(username)){
                return user1;
            }
        }
        return null;
    }

    public void updateUser(User olduser) {
        for (User i : users) {
            if (olduser.getUsername().equals(i.getUsername())) {
                i.setName(olduser.getName());
                break;
            }
        }
    }

    public void delUser(String username) {
        for (User i : users) {
            if (username.equals(i.getUsername())) {
                users.remove(i);
                break;
            }
        }
    }
}
