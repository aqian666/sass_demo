package com.example.test.pojo;

public class Order extends Car{


    private String orderNo;


    private String orderCarName;

    private String status;

    public Order(String orderNo, String orderCarName, String status) {
        this.orderNo = orderNo;
        this.orderCarName = orderCarName;
        this.status = status;
    }


    public Order(Car car, String orderNo, String orderCarName, String status) {
        super(car.getId() , car.getCarName(), car.getMake(), car.getPrice());
        this.orderNo = orderNo;
        this.orderCarName = orderCarName;
        this.status = status;
    }


    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderCarName() {
        return orderCarName;
    }

    public void setOrderCarName(String orderCarName) {
        this.orderCarName = orderCarName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
