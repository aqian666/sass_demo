package com.example.test.pojo;

import java.util.ArrayList;
import java.util.List;

public class Car {

    private int id;

    private String carName;
    private String make;
    private Double price;

    public List<Car> cars = new ArrayList<>();

    public Car(int id , String carName, String make, Double price) {
        this.id = id;
        this.carName = carName;
        this.make = make;
        this.price = price;
    }

    public Car() {
    }


    public List<Car> getCar() {
        cars.add(new Car(1,"红旗","中国",259999.9));
        cars.add(new Car(2,"宝马","德国",209999.9));
        cars.add(new Car(3,"丰田","日本",109999.9));
        return cars;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
